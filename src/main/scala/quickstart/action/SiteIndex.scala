package quickstart.action

import xitrum.annotation.GET

import scala.util.{Failure, Success}

@GET("")
class SiteIndex extends DefaultLayout{
  def execute() {
    ComponentsAccumulate.accum[FutureComponent] (("ProfileBlock", newComponent[ProfileBlock])) onComplete {
      case Success(seq)=>{
        seq foreach {x=>at(x._1) = x._2}
        respondView
      }
      case Failure(ex) =>{
        log error  s"$ex"
        respondView
      }
    }
  }
}