package quickstart.action

import xitrum.Component

/**
  * Created by luger on 27.02.16.
  */
class Header extends Component with xitrum.Log{
  def render(user:String) = {
    // Render associated view template, e.g. CompoWithView.jade
    // Note that this is renderView, not respondView!
    val tag = paramo[String]("tags").getOrElse("")
    log info s"tags:$tag $user"
    at("tags") = List("расписание")
    at("noauth") = !(session isDefinedAt ("userId"))
    renderFragment("header")
  }
}