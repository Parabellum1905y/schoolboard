package quickstart.action

import xitrum.Action
import xitrum.annotation.GET

@GET("about")
class AboutUs extends DefaultLayout{
  def execute() {
    respondView() 
  }
}
