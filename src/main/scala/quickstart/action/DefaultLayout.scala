package quickstart.action

import xitrum.FutureAction

trait DefaultLayout extends FutureAction {
  override def layout = renderViewNoLayout[DefaultLayout]()
}
