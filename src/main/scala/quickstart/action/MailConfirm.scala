package quickstart.action

import school.controllers.ConfirmRegUser
import xitrum.Component

/**
  * Created by luger on 27.02.16.
  */
class MailConfirm extends Component with xitrum.Log{
  def render(params:Array[(String, Any)]) = {
    at("rurl") = absUrl[ConfirmRegUser](params:_*)
    renderFragment("regMailConfirm")
  }
}