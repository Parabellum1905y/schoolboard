package quickstart.action

import school.dao.usersDAO.UserDao
import school.model.users.User
import xitrum.FutureAction

import scala.concurrent.Future

/**
  * Created by luger on 05.03.16.
  */
trait PostsHtml extends FutureAction

trait PostHtml extends FutureAction

class Posts(mode:String) extends FutureComponent{
  def render(ats:String):Future[(String, String)] = {
    at("noauth") = !(session isDefinedAt ("userId"))
    mode match {
      case "own"  =>

      case "latest"=>
    }
    UserDao getUserById (sessiono[Long]("userId").getOrElse(-1L)) map {
      user => {
        at("user") = user.getOrElse(User("", Some(""), ""))
        val r = renderView()
        (ats, r)
      }
    }
  }

  def render(ats:String, x:Any*):Future[(String, String)] = {
    at("noauth") = !(session isDefinedAt ("userId"))
    UserDao getUserById sessiono[Long]("userId").getOrElse(-1L) map{
      user =>{
        at("user") = user.getOrElse(User("", Some(""), ""))
        val r = renderView()
        (ats, r)
      }
    }
  }
}

/**
onComplete{
      case Success(user)=>{
          at("user") = user.getOrElse(User("", Some(""), ""))
          val r = renderView()
          log info s"$r"
          r
      }
      case Failure(ex) =>{
          log error  s"$ex"
          at("user") = User("", Some(""), "")
          val r = renderView()
          log info s"$r"
          r
        }
      }
  */
/*class ProfileBlock extends FutureAction with xitrum.Log{
  def execute() = {
    // Render associated view template, e.g. CompoWithView.jade
    // Note that this is renderView, not respondView!
    at("noauth") = !(session isDefinedAt ("userId"))
    UserDao getUserById sessiono[Long]("userId").getOrElse(-1L) onComplete{
      case Success(user)=>{
        at("user") = user.getOrElse(User("", Some(""), ""))
        respondView("profileBlock")
      }
      case Failure(ex) =>{
        log error  s"$ex"
        at("user") = User("", Some(""), "")
        respondView("profileBlock")
      }
    }
  }
}*/