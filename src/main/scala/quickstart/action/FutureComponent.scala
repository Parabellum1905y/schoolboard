package quickstart.action

import xitrum.Component
import scala.concurrent.Future

/**
  * Created by luger on 06.03.16.
  */
trait FutureComponent extends Component with xitrum.Log{
  def render(s:String): Future[(String, String)]
  def render(s:String, x:Any*):Future[(String, String)]
}
