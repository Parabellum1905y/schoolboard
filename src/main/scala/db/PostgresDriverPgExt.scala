package db
import com.github.tminglei.slickpg._

trait PostgresDriverPg  extends ExPostgresDriver
                              with PgDateSupport
                              with PgDate2Support
                              with PgArraySupport
                              with PgRangeSupport{

   override val api = MyAPI
   object MyAPI extends API with ArrayImplicits
                           with DateTimeImplicits
                           with RangeImplicits{
    implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)
  }
}

object PostgresDriverPgExt extends PostgresDriverPg