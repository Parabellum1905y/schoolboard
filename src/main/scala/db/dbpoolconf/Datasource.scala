package db.dbpoolconf

import org.apache.commons.dbcp2.BasicDataSource
import slick.jdbc.JdbcBackend.Database
import com.zaxxer.hikari.HikariDataSource

class DatasourceConf(name:String) {
  lazy val poolConfig = if (xitrum.Config.application.hasPath("db"))
                          Some (xitrum.Config.application.getConfig("db"))
                        else None
  lazy val nConfig =  if (poolConfig.get.hasPath(name))
                          Some (poolConfig.get.getConfig(name))
                        else None            
  def database = {
 
    val cpds = new HikariDataSource
    cpds.setDriverClassName(nConfig.get.getString("driver"))
    cpds.setJdbcUrl(nConfig.get.getString("url"))
    cpds.setUsername(nConfig.get.getString("user"))
    cpds.setPassword(nConfig.get.getString("pwd"))
    cpds.setMinimumIdle(nConfig.get.getString("min_pool_size").toInt)
    cpds.setConnectionTestQuery(nConfig.get.getString("check_query"))
    cpds.setMaximumPoolSize(nConfig.get.getString("max_pool_size").toInt)
//    cpds.setMaxOpenPreparedStatements(nConfig.get.getString("max_statements").toInt)
    Database.forDataSource(cpds)
//TODO java.lang.IllegalStateException: ConcurrentBag has been closed, ignoring add()

/*  
    val cpds = new BasicDataSource
    cpds.setDriverClassName(nConfig.get.getString("driver"))
    cpds.setUrl(nConfig.get.getString("url"))
    cpds.setUsername(nConfig.get.getString("user"))
    cpds.setPassword(nConfig.get.getString("pwd"))
    cpds.setInitialSize(nConfig.get.getString("min_pool_size").toInt)
    cpds.setMinIdle(nConfig.get.getString("min_pool_size").toInt)
    cpds.setValidationQuery(nConfig.get.getString("check_query"))
    cpds.setMaxTotal(nConfig.get.getString("max_pool_size").toInt)
    cpds.setMaxOpenPreparedStatements(nConfig.get.getString("max_statements").toInt)
    Database.forDataSource(cpds)
*/
  }
}

object Datasource{
  def apply (name:String):DatasourceConf = {
    new DatasourceConf(name)
  }  
}