package mailer

/**
  * Created by luger on 02.03.16.
  */
class MailerConfRef {

  lazy val mailConfig = if (xitrum.Config.application.hasPath("mail"))
                            Some (xitrum.Config.application.getConfig("mail"))
                        else None
  def config(param:String) = mailConfig match {
    case Some(x)=>x.getString(param)
    case None   =>""
  }

  def host         = config("mail.smtp.host")
  def port         = config("mail.smtp.port")
  def userName     = config("userName")
  def userPassword = config("userPassword")
  def auth         = config("mail.smtp.auth")
  def timeout      = config("mail.smtp.timeout")
  def rootLink     = config("root.link")
  def from         = config ("mail.from")
}

object MailerConf{
  def apply:MailerConfRef = new MailerConfRef
}