package school.controllers

import quickstart.action.AnotherProfileBlock
import school.dao.usersDAO.UserDao
import xitrum.FutureAction
import xitrum.annotation.{GET, POST, Swagger}
import scala.util.{Success, Failure}
import school.model.users.{User, NoneUser}

trait Users extends FutureAction with xitrum.Log

//class UsersRegistration extends Users{
//  
//}

@GET("profile/:id<[0-9]+>", "profile/:id<[0-9]+>.:path")
class UsersProfile extends Users{
  def execute {
    val id = paramo[Long]("id").getOrElse(-1L)
    val path = paramo[String]("path").getOrElse("json")
    val user = UserDao getUserByIdSafe (id)
    user.onComplete { 
      case Success(x) => {
        val n = NoneUser
        val us = x.getOrElse(NoneUser)
        log debug s"user:${us}" 
        if (path == "json") respondJson(Map("user"->us))
        else if (path == "html"){
          at("user") = us
          respondView[AnotherProfileBlock] ()
        }
        else respond500Page()
      }
      case Failure(err) => {
        log info ("error ", err)
        respond404Page()
      }
    }
  }
}

@POST("profile/update")
class UsersProfileUpdate extends Users{
  def execute {
    val isLoggedOn = session isDefinedAt ("userId")
    if (isLoggedOn) {
      val sessionUserId = sessiono[Long]("userId").get
      val avatarFileId = paramo[Long]("avatarFileId").getOrElse(-1L)
      val name = paramo[String]("name").getOrElse("")
      val surname = paramo[String]("surname").getOrElse("")
      val aboutMe = paramo[String]("aboutMe").getOrElse("")
      val birthDate = paramo[String]("birthdate").getOrElse("")
      if (name.isEmpty)respondJson(Map("status"->"error", "data"->"NAME_NON_EMPTY"))
      else
        UserDao.update {
          User("", Some(""), "",
            userId = sessionUserId, avatarFileId = Some(avatarFileId),
            name = Some(name), surname = Some(surname), aboutMe = Some(aboutMe))
        } onComplete{
          case Success (0) =>
            respondJson(Map("status"->"error", "data"->"USER_NOT_UPDATED"))
          case Success(1) =>
            respondJson(Map("status"->"success"))
          case Failure(ex) =>
            log error s"$ex"
            respondJson(Map("status"->"error", "data"->"USER_NOT_UPDATED"))
        }
    } else respondJson(Map("status"->"error", "data"->"USER_NOT_LOGGED"))
  }
}