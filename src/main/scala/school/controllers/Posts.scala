package school.controllers

import java.time.{ZoneId, ZoneOffset, LocalDateTime, Instant}
import java.util.TimeZone

import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import quickstart.action.{PostsHtml, PostHtml}
import school.dao.posts.PostsDao
import school.dao.usersDAO.UserDao
import school.model.posts.PostsRow
import school.model.tags.TagsRow
import school.model.users.User
import xitrum.FutureAction
import xitrum.annotation.{POST, Swagger, GET}
import scala.util.{Try, Success, Failure}

trait Posts extends FutureAction with xitrum.Log

//class UsersRegistration extends Users{
//  
//}

@POST("post/new")
class NewPost extends Posts{
  def execute {
    val isLoggedOn = session isDefinedAt "userId"
    if (isLoggedOn) {
      val sessionUserId = sessiono[Long]("userId").get
      val pTagsText = paramo[String]("tags").getOrElse("")
      val pTheme = paramo[String]("theme").getOrElse("")
      val pMessage = paramo[String]("message").getOrElse("")
      (pTagsText, pTheme, pMessage) match {
        case (_, _, "") => respondJson(Map("status" -> "error", "data" -> "EMPTY_MESSAGE"))
        case (_, "", _) => respondJson(Map("status" -> "error", "data" -> "EMPTY_THEME"))
        case ("", _, _) => respondJson(Map("status" -> "error", "data" -> "EMPTY_TAGS"))
        case (tagsText, theme, message) => {
          val tags = tagsText split "," map(_.trim)
          log info s"$message"
          val whiteList = Whitelist.relaxed
          whiteList.addAttributes(":all", "class").
          addAttributes("img", "src", "width", "height", "style").
          preserveRelativeLinks(true)
          val sanitizedMessage = Jsoup.clean(message, whiteList).replaceAll("\n", "")
          log info s"$sanitizedMessage"
          val sanitizedTheme = Jsoup.clean(theme, Whitelist.simpleText)
          val post = PostsRow(postid = 0L, userid = sessionUserId,
                              theme = Some(sanitizedTheme), message = Some(sanitizedMessage))
          PostsDao createWithTagsRet (post, tags) onComplete {
            case Success(None)   =>respondJson(Map("status" -> "error", "data" -> "UNKNOWN_ERROR"))
            case Success(Some(x))=>respondJson(Map("status" -> "success", "data" -> x))
            case Failure(ex)     =>
              log error s"$ex"
              respondJson(Map("status" -> "error", "data" -> "UNKNOWN_ERROR"))
          }
        }
      }
    }else respondJson(Map("status" -> "error", "data" -> "USER_NOT_LOGGED"))
  }
}

trait PostsComplete extends Posts{
  def onCompletePostsQueryJson (trying: Try[Seq[(Option[PostsRow], Option[Option[TagsRow]])]]) = trying match{
    case Success(x) =>
      val listOfPosts = x.groupBy ({ t => t._1 }).map ({ y=>(y._1, y._2.map(_._2))}).toList.map{
        tuple=>Map("post"->tuple._1, "tags"->tuple._2)
      }
      //listOfPosts
      at("posts") = listOfPosts
      respondJson("status"->"success", "data"->listOfPosts)
    case Failure(err) =>
      log error s"$err"
      respondJson("status"->"error")
  }

  def onCompletePostsQueryHtml (trying: Try[Seq[(Option[PostsRow], Option[Option[TagsRow]])]]) = trying match{
    case Success(x) => {
      val listOfPosts = x.groupBy ({ t => t._1 }).map ({ y=>(y._1, y._2.map(_._2))}).toList
        /*.map{
        tuple=>Map("post"->tuple._1, "tags"->tuple._2)
      }*/
      //listOfPosts
      at("posts") = listOfPosts
      respondView[PostsHtml]()
    }
    case Failure(err) => {
      log error s"$err"
      at("posts") = Nil
      respondView[PostsHtml]()
    }
  }

  def onCompletePostsQueryHtmlForTagText (trying: Try[Seq[(Option[PostsRow], Option[TagsRow])]]) = trying match{
    case Success(x) => {
      val listOfPosts = x.groupBy ({ t => t._1 }).map ({ y=>(y._1, y._2.map({tag=>Some(tag._2)}))}).toList
      /*.map{
      tuple=>Map("post"->tuple._1, "tags"->tuple._2)
    }*/
      //listOfPosts
      at("posts") = listOfPosts
      respondView[PostsHtml]()
    }
    case Failure(err) => {
      log error s"$err"
      at("posts") = Nil
      respondView[PostsHtml]()
    }
  }

  def onCompletePostQueryHtml (trying: Try[Seq[(Option[PostsRow], Option[Option[TagsRow]], User)]]) = trying match{
    case Success(x) => {
      val post = x.groupBy ({ t => t._1 }).map ({ y=>(y._1, y._2.map(_._2), y._2.map(_._3).head)}).toList.head
      /*.map{
      tuple=>Map("post"->tuple._1, "tags"->tuple._2)
    }*/
      //listOfPosts
      at("post") = post
      respondView[PostHtml]()
    }
    case Failure(err) => {
      log error s"$err"
      at("post") = (None, None)
      respondView[PostHtml]()
    }
  }
}

@GET ("posts/last", "posts/last.:path", "posts/last/:mode<(me)>", "posts/last.:path/:mode<(me)>")
class LoadLastPosts extends PostsComplete{
  def execute =
    (paramo[String]("mode").getOrElse(""), paramo[String]("path").getOrElse("")) match {
      case ("me", path) =>
        val isLoggedOn = session isDefinedAt "userId"
        if (isLoggedOn) {
          if (path == "html")PostsDao.getLast(sessiono[Long]("userId").get, "own").onComplete(onCompletePostsQueryHtml)
          else PostsDao.getLast(sessiono[Long]("userId").get, "own").onComplete(onCompletePostsQueryJson)
        }else
          if (path == "html")PostsDao.getLast(0L, "").onComplete(onCompletePostsQueryHtml)
          else PostsDao.getLast(0L, "").onComplete(onCompletePostsQueryJson)
      case (_, path) =>
        if (path == "html")PostsDao.getLast(0L, "").onComplete(onCompletePostsQueryHtml)
        else PostsDao.getLast(0L, "").onComplete(onCompletePostsQueryJson)
    }
}

@GET ("posts/fortagid", "posts/fortagid.:path", "posts/fortagid/:mode<(me)>", "posts/fortagid.:path/:mode<(me)>")
class LoadLastPostsForTagId extends PostsComplete{
  def execute = {
    log info s"ololo:ololo"
    val tagId = paramo[Long]("tagId").getOrElse(-1L)
    (paramo[String]("mode").getOrElse(""), paramo[String]("path").getOrElse("")) match {
      case ("me", path) =>
        val isLoggedOn = session isDefinedAt "userId"
        if (isLoggedOn) {
          if (path == "html") PostsDao.getLastForTag(sessiono[Long]("userId").get, "own", tagId).
                              onComplete(onCompletePostsQueryHtmlForTagText)
          else PostsDao.getLastForTag(sessiono[Long]("userId").get, "own", tagId).
                              onComplete(onCompletePostsQueryHtmlForTagText)
        } else {
          if (path == "html") PostsDao.getLastForTag(0L, "", tagId).
                              onComplete(onCompletePostsQueryHtmlForTagText)
          else PostsDao.getLastForTag(0L, "", tagId).
                              onComplete(onCompletePostsQueryHtmlForTagText)
        }
      case (_, path) =>
        if (path == "html") PostsDao.getLastForTag(0L, "", tagId).onComplete(onCompletePostsQueryHtmlForTagText)
        else PostsDao.getLastForTag(0L, "", tagId).onComplete(onCompletePostsQueryHtmlForTagText)
    }
  }
}

  //TODO make this for JSON
@GET ("posts/fortag", "posts/fortag.:path", "posts/fortag/:mode<(me)>", "posts/fortag.:path/:mode<(me)>")
class LoadLastPostsForTag extends PostsComplete{
  def execute = {
    val tagText = paramo[String]("tag").getOrElse("")
    (paramo[String]("mode").getOrElse(""), paramo[String]("path").getOrElse("")) match {
      case ("me", path) =>
        val isLoggedOn = session isDefinedAt "userId"
        if (isLoggedOn) {
          if (path == "html") PostsDao.getLastForTagText(sessiono[Long]("userId").get, "own", tagText).
                                  onComplete(onCompletePostsQueryHtmlForTagText)
          else PostsDao.getLastForTagText(sessiono[Long]("userId").get, "own", tagText).
                                  onComplete(onCompletePostsQueryHtmlForTagText)
        } else if (path == "html") PostsDao.getLastForTagText(0L, "", tagText).
                                  onComplete(onCompletePostsQueryHtmlForTagText)
        else PostsDao.getLastForTagText(0L, "", tagText).onComplete(onCompletePostsQueryHtmlForTagText)
      case (_, path) =>
        if (path == "html") PostsDao.getLastForTagText(0L, "", tagText).onComplete(onCompletePostsQueryHtmlForTagText)
        else PostsDao.getLastForTagText(0L, "", tagText).onComplete(onCompletePostsQueryHtmlForTagText)
    }
  }
}

@GET ("posts/next/:lastDate", "posts/next/:lastDate/:mode<(me)>")
class LoadNextPosts extends PostsComplete{
  def execute =
    paramo[String]("mode").getOrElse("") match {
      case "me" =>
        val isLoggedOn = session isDefinedAt "userId"
        if (isLoggedOn) {
          PostsDao.getLast(sessiono[Long]("userId").get, "own").onComplete(onCompletePostsQueryJson)
        }
      case _ => PostsDao.getLast(0L, "").onComplete(onCompletePostsQueryJson)
    }
}

@GET ("posts/prev/:lastDate", "posts/prev/:lastDate/:mode<(me)>")
class LoadPrevPosts extends PostsComplete{
  def execute = {
    //val dTF2 = DateTimeFormatter.ofPattern("dd.MM.uuuu-hh:mm:ss")
    val zoneId = ZoneId.systemDefault
    val lastDateS = paramo[Long]("lastDate").
        getOrElse(LocalDateTime.now.atZone(ZoneId.systemDefault).toEpochSecond)
    if (lastDateS<0) respondJson(Map("status" -> "error", "data" -> "INVALID_DATE"))
    else {
      val lastDate: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(lastDateS), TimeZone
        .getDefault().toZoneId)
      paramo[String]("mode").getOrElse("") match {
        case "me" =>
          val isLoggedOn = session isDefinedAt "userId"
          if (isLoggedOn) {
            PostsDao.getPrev(sessiono[Long]("userId").get, "own", lastDate).onComplete(onCompletePostsQueryHtml)
          }
        case _ => PostsDao.getPrev(0L, "", lastDate).onComplete(onCompletePostsQueryHtml)
      }
    }
  }
}

@GET ("post/:id")
class LoadPost extends PostsComplete{
  def execute =
    paramo[Long]("id").getOrElse(-1L) match {
      case -1L =>
        at("posts") = (None, None)
        respondView[PostHtml]()
      case x => PostsDao.getPostByIdWithTagsUser(x).onComplete(onCompletePostQueryHtml)
    }
}