package school.controllers

import java.nio.charset.StandardCharsets
import javax.mail.internet.{InternetAddress => iaddr, MimeBodyPart}

import courier._, Defaults._, mailer.MailerConf
import quickstart.action.{MailConfirm, SiteIndex}
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.{Success, Failure}
import school.dao.usersDAO.UserDao
import school.model.users.User
import xitrum.FutureAction
import xitrum.annotation.{POST, GET}

/**
  * Created by luger on 28.02.16.
  */
trait Login extends FutureAction with xitrum.Log{
  lazy val userman = UserDao
  lazy val mailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$".r
  lazy val mailPatternReg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
}

@POST("login")
class LogUser extends Login{
  override def execute {
    val l = paramo[String]("login")
    val p = paramo[String]("password")
    log info s"${session isDefinedAt ("userId")}"
    (l, p) match {
      case (None, _)=>respondJson(Map("status"->"error", "data"->"INVALID_LOGIN"))
      case (Some(x), _) if(!x.matches(mailPatternReg))=>respondJson(Map("status"->"error", "data"->"INVALID_LOGIN"))
      case (_, None)=>respondJson(Map("status"->"error", "data"->"INVALID_PWD"))
      case (Some(login), Some(pwd))=> userman.getUserByLogin(login, pwd).onComplete{
        case Success(None)=>respondJson(Map("status"->"error", "data"->"USER_NOT_FOUND"))
        case Success(Some(x))=>x match {
          case x if (x.enabled==1)=>{
            session ("userId") = x.userId
            respondJson(Map("status"->"success", "data"->"OK"))
          }
          case _ =>respondJson(Map("status"->"error", "data"->"USER_NOT_ENABLED"))
        }
        case Failure(ex)=>{
          log error s"$ex"
          respondJson(Map("status"->"error", "data"->"USER_NOT_FOUND"))
        }
      }
    }
  }
}

@POST("logout")
class LogoutUser extends Login{
  override def execute {
    if (session isDefinedAt ("userId"))session remove "userId"
    respondJson(Map("status"->"success", "data"->"OK"))
  }
}

@POST("signin")
class RegUser extends Login{
  def getOrCreateUser (login:String, pwd:String):Future[(Long, String)] = for {
    existingUser: Option[User] <- userman.getUserByLogin(login)
    result<- existingUser.fold(userman createAndLog User(login, Some(pwd), login, userMail = Some(login), enabled = 0)){ _=>Future{(-1L, "")}}
  } yield result

  def sendConfirmMail (login:String, hash:String) = {
    val conf = MailerConf.apply
    val mailer = Mailer(conf.host, conf.port.toInt)
      .auth(conf.auth.toBoolean)
      .as(conf.userName, conf.userPassword)
      .startTtls(true)()
    val future = mailer(Envelope.from(new iaddr(conf.userName, conf.from, "UTF-8"))
      .to(new iaddr(login, login, "UTF-8"))
      .subject("Подтверждение пароля")
      .content(Multipart()
          .add({
            val part = new MimeBodyPart
            part.setText(newComponent[MailConfirm]().render(Array("code"->hash)),
                          StandardCharsets.UTF_8.name, "html")
            part
          })))
    /*.onSuccess {
      case x => log info s"delivered report:$x"
    }*/
    Await.ready(future, conf.timeout.toInt.seconds)
  }

  override def execute {
    val l = paramo[String]("login")
    val p = paramo[String]("password")
    val ch = paramo[String]("passwordconfirm")
    log info s"${session isDefinedAt ("userId")}"
    (l, p, ch) match {
      case (None, _, _)=>respondJson(Map("status"->"error", "data"->"INVALID_LOGIN"))
      case (_, None, _)=>respondJson(Map("status"->"error", "data"->"INVALID_PWD"))
      case (_, _, None)=>respondJson(Map("status"->"error", "data"->"INVALID_PWD_CONFIRM"))
      case (_, Some(x), Some(y)) if(!(x == y))=>respondJson(Map("status"->"error", "data"->"NOT_EQ_PWD_CONFIRM"))
      case (Some(login), Some(pwd), Some(check))=>{
        getOrCreateUser(login, pwd).onComplete{
          case Success((-1, ""))=>respondJson(Map("status"->"error", "data"->"ALREADY_FOUND"))
          case Success(x)       =>{
            sendConfirmMail (login, x._2)
            respondJson(Map("status"->"success", "data"->"OK"))
          }
          case Failure(ex)=>{
            log error s"$ex"
            respondJson(Map("status"->"error", "data"->"USER_NOT_CREATED"))
          }
        }
      }
    }
  }
}

@GET("confirm")
class ConfirmRegUser extends Login{
  def execute = paramo[String]("code") match {
    case None      => redirectTo[SiteIndex]()
    case Some(code)=> userman activateUser(code) onComplete {
      case Success(count) => if (count>0)redirectTo[SiteIndex]("msg"->"finish_activation")
                              else redirectTo[SiteIndex]("msg"->"failed_activation")
      case Failure(ex) =>{
        log error s"$ex"
        redirectTo[SiteIndex]("msg"->"failed_activation")
      }
    }
  }
}
