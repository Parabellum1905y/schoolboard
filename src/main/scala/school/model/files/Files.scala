package school.model.files

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
import java.time.LocalDateTime

  /** Entity class storing rows of table Files
   *  @param fileid Database column fileid SqlType(bigserial), AutoInc, PrimaryKey
   *  @param userId Database column userlogin SqlType(varchar), Length(200,true), Default(None)
   *  @param name Database column name SqlType(varchar), Length(200,true), Default(None)
   *  @param realname Database column realname SqlType(varchar), Length(200,true), Default(None)
   *  @param size Database column size SqlType(int8), Default(None)
   *  @param mediaformat Database column mediaformat SqlType(int2), Default(None)
   *  @param isdirectory Database column isdirectory SqlType(int2), Default(None)
   *  @param parentid Database column parentid SqlType(int8), Default(None)
   *  @param createdate Database column createdate SqlType(timestamp)
   *  @param depth Database column depth SqlType(int2), Default(None)
   *  @param hidden Database column hidden SqlType(int2), Default(None)
   *  @param readonly Database column readonly SqlType(int2), Default(None)
   *  @param state Database column state SqlType(int2), Default(None)
   *  @param width Database column width SqlType(int2), Default(None)
   *  @param height Database column height SqlType(int2), Default(None) */
  case class FilesRow(fileid: Long = 0L,
                      userId: Long,
                      name: Option[String] = None,
                      realname: Option[String] = None,
                      size: Option[Long] = None,
                      mediaformat: Option[Short] = None,
                      isdirectory: Option[Short] = None,
                      parentid: Option[Long] = None,
                      createdate: Option[LocalDateTime] = Some(LocalDateTime.now),
                      depth: Option[Short] = None,
                      hidden: Option[Short] = None,
                      readonly: Option[Short] = None,
                      state: Option[Short] = None,
                      width: Option[Short] = None,
                      height: Option[Short] = None)
  
  class Files(_tableTag: Tag) extends Table[FilesRow](_tableTag, "files") {
    def * = (fileid, userId, name, realname, size, mediaformat, isdirectory, parentid, createdate, depth, hidden, readonly, state, width, height) <> (FilesRow.tupled, FilesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(fileid), userId, name, realname, size, mediaformat, isdirectory, parentid, createdate, depth, hidden, readonly, state, width, height).shaped.<>({r=>import r._; _1.map(_=> FilesRow.tupled((_1.get, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    /** Table description of table files. Objects of this class serve as prototypes for rows in queries. */

    /** Database column fileid SqlType(bigserial), AutoInc, PrimaryKey */
    val fileid: Rep[Long] = column[Long]("fileid", O.AutoInc, O.PrimaryKey)
    /** Database column userlogin SqlType(varchar), Length(200,true), Default(None) */
    val userId: Rep[Long] = column[Long]("userid")
    /** Database column name SqlType(varchar), Length(200,true), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Length(200,varying=true), O.Default(None))
    /** Database column realname SqlType(varchar), Length(200,true), Default(None) */
    val realname: Rep[Option[String]] = column[Option[String]]("realname", O.Length(200,varying=true), O.Default(None))
    /** Database column size SqlType(int8), Default(None) */
    val size: Rep[Option[Long]] = column[Option[Long]]("size", O.Default(None))
    /** Database column mediaformat SqlType(int2), Default(None) */
    val mediaformat: Rep[Option[Short]] = column[Option[Short]]("mediaformat", O.Default(None))
    /** Database column isdirectory SqlType(int2), Default(None) */
    val isdirectory: Rep[Option[Short]] = column[Option[Short]]("isdirectory", O.Default(None))
    /** Database column parentid SqlType(int8), Default(None) */
    val parentid: Rep[Option[Long]] = column[Option[Long]]("parentid", O.Default(None))
    /** Database column createdate SqlType(timestamp) */
    val createdate: Rep[Option[java.time.LocalDateTime]] = column[Option[java.time.LocalDateTime]]("createdate")
    /** Database column depth SqlType(int2), Default(None) */
    val depth: Rep[Option[Short]] = column[Option[Short]]("depth", O.Default(None))
    /** Database column hidden SqlType(int2), Default(None) */
    val hidden: Rep[Option[Short]] = column[Option[Short]]("hidden", O.Default(None))
    /** Database column readonly SqlType(int2), Default(None) */
    val readonly: Rep[Option[Short]] = column[Option[Short]]("readonly", O.Default(None))
    /** Database column state SqlType(int2), Default(None) */
    val state: Rep[Option[Short]] = column[Option[Short]]("state", O.Default(None))
    /** Database column width SqlType(int2), Default(None) */
    val width: Rep[Option[Short]] = column[Option[Short]]("width", O.Default(None))
    /** Database column height SqlType(int2), Default(None) */
    val height: Rep[Option[Short]] = column[Option[Short]]("height", O.Default(None))
  }

  object Files{
    lazy val Files = new TableQuery(tag => new Files(tag))
    def get = Files
  }
  
  /** Entity class storing rows of table Fileaccesses
   *  @param accessid Database column accessid SqlType(bigserial), AutoInc, PrimaryKey
   *  @param fileid Database column fileid SqlType(int8), Default(None)
   *  @param membertype Database column membertype SqlType(int2), Default(None)
   *  @param memberid Database column memberid SqlType(int8), Default(None)
   *  @param isowner Database column isowner SqlType(int2), Default(None)
   *  @param statususe Database column statususe SqlType(int2), Default(None) */
  case class FileaccessesRow(accessid: Long, fileid: Option[Long] = None, membertype: Option[Short] = None, memberid: Option[Long] = None, isowner: Option[Short] = None, statususe: Option[Short] = None)

  /** Table description of table fileaccesses. Objects of this class serve as prototypes for rows in queries. */
  class Fileaccesses(_tableTag: Tag) extends Table[FileaccessesRow](_tableTag, "fileaccesses") {
    def * = (accessid, fileid, membertype, memberid, isowner, statususe) <> (FileaccessesRow.tupled, FileaccessesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(accessid), fileid, membertype, memberid, isowner, statususe).shaped.<>({r=>import r._; _1.map(_=> FileaccessesRow.tupled((_1.get, _2, _3, _4, _5, _6)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    /** GetResult implicit for fetching FileaccessesRow objects using plain SQL queries */
    implicit def GetResultFileaccessesRow(implicit e0: GR[Long], e1: GR[Option[Long]], e2: GR[Option[Short]]): GR[FileaccessesRow] = GR{
      prs => import prs._
      FileaccessesRow.tupled((<<[Long], <<?[Long], <<?[Short], <<?[Long], <<?[Short], <<?[Short]))
    }

    /** Database column accessid SqlType(bigserial), AutoInc, PrimaryKey */
    val accessid: Rep[Long] = column[Long]("accessid", O.AutoInc, O.PrimaryKey)
    /** Database column fileid SqlType(int8), Default(None) */
    val fileid: Rep[Option[Long]] = column[Option[Long]]("fileid", O.Default(None))
    /** Database column membertype SqlType(int2), Default(None) */
    val membertype: Rep[Option[Short]] = column[Option[Short]]("membertype", O.Default(None))
    /** Database column memberid SqlType(int8), Default(None) */
    val memberid: Rep[Option[Long]] = column[Option[Long]]("memberid", O.Default(None))
    /** Database column isowner SqlType(int2), Default(None) */
    val isowner: Rep[Option[Short]] = column[Option[Short]]("isowner", O.Default(None))
    /** Database column statususe SqlType(int2), Default(None) */
    val statususe: Rep[Option[Short]] = column[Option[Short]]("statususe", O.Default(None))
    
    lazy val filesFk = foreignKey("files_fileid_fkey", fileid, Files.Files)(r => Rep.Some(r.fileid), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }

object Fileaccesses{
  lazy val Fileaccesses = new TableQuery(tag => new Fileaccesses(tag))
  def get = Fileaccesses
}
