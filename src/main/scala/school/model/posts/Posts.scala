package school.model.posts

import java.time.LocalDateTime

import school.model.users.Users
import school.model.tags.Tags
import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._

/** Entity class storing rows of table Posts
 *
  *  @param postid Database column postid SqlType(bigserial), AutoInc, PrimaryKey
  *  @param userid Database column userid SqlType(int8)
  *  @param theme Database column theme SqlType(varchar), Length(2048,true), Default(None)
  *  @param message Database column message SqlType(text), Default(None)
  *  @param createdate Database column createdate SqlType(timestamp) */
case class PostsRow(postid: Long,
                    userid: Long,
                    theme: Option[String] = None,
                    message: Option[String] = None,
                    createdate: LocalDateTime = LocalDateTime.now)
/** Table description of table posts. Objects of this class serve as prototypes for rows in queries. */
class Posts(_tableTag: Tag) extends Table[PostsRow](_tableTag, "posts") {
  def * = (postid, userid, theme, message, createdate) <> (PostsRow.tupled, PostsRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(postid), Rep.Some(userid), theme, message, Rep.Some(createdate)).shaped.<>({r=>import r._; _1.map(_=> PostsRow.tupled((_1.get, _2.get, _3, _4, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching PostsRow objects using plain SQL queries */
  implicit def GetResultPostsRow(implicit e0: GR[Long], e1: GR[Option[String]], e2: GR[java.time.LocalDateTime]): GR[PostsRow] = GR{
    prs => import prs._
      PostsRow.tupled((<<[Long], <<[Long], <<?[String], <<?[String], <<[java.time.LocalDateTime]))
  }

  /** Database column postid SqlType(bigserial), AutoInc, PrimaryKey */
  val postid: Rep[Long] = column[Long]("postid", O.AutoInc, O.PrimaryKey)
  /** Database column userid SqlType(int8) */
  val userid: Rep[Long] = column[Long]("userid")
  /** Database column theme SqlType(varchar), Length(2048,true), Default(None) */
  val theme: Rep[Option[String]] = column[Option[String]]("theme", O.Length(2048,varying=true), O.Default(None))
  /** Database column message SqlType(text), Default(None) */
  val message: Rep[Option[String]] = column[Option[String]]("message", O.Default(None))
  /** Database column createdate SqlType(timestamp) */
  val createdate: Rep[java.time.LocalDateTime] = column[java.time.LocalDateTime]("createdate")

  /** Foreign key referencing Users (database name posts_userid_fkey) */
  lazy val usersFk = foreignKey("posts_userid_fkey", userid, Users.get)(r => r.userId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}

object Posts{
  /** Collection-like TableQuery object for table Posts */
  lazy val Posts = new TableQuery(tag => new Posts(tag))
  def get = Posts
}

/** Entity class storing rows of table PostsTags
 *
  *  @param postid Database column postid SqlType(int8)
  *  @param tagid Database column tagid SqlType(int8) */
case class PostsTagsRow(postid: Long, tagid: Long)

/** Table description of table posts_tags. Objects of this class serve as prototypes for rows in queries. */
class PostsTags(_tableTag: Tag) extends Table[PostsTagsRow](_tableTag, "posts_tags") {
  def * = (postid, tagid) <> (PostsTagsRow.tupled, PostsTagsRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(postid), Rep.Some(tagid)).shaped.<>({r=>import r._; _1.map(_=> PostsTagsRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching PostsTagsRow objects using plain SQL queries */
  implicit def GetResultPostsTagsRow(implicit e0: GR[Long]): GR[PostsTagsRow] = GR{
    prs => import prs._
      PostsTagsRow.tupled((<<[Long], <<[Long]))
  }

  /** Database column postid SqlType(int8) */
  val postid: Rep[Long] = column[Long]("postid")
  /** Database column tagid SqlType(int8) */
  val tagid: Rep[Long] = column[Long]("tagid")

  /** Foreign key referencing Posts (database name posts_tags_postid_fkey) */
  lazy val postsFk = foreignKey("posts_tags_postid_fkey", postid, Posts.get)(r => r.postid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing Tags (database name posts_tags_tagid_fkey) */
  lazy val tagsFk = foreignKey("posts_tags_tagid_fkey", tagid, Tags.get)(r => r.tagid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

  /** Uniqueness Index over (postid,tagid) (database name tag_for_post) */
  val index1 = index("tag_for_post", (postid, tagid), unique=true)
}

object PostsTags{
  /** Collection-like TableQuery object for table PostsTags */
  lazy val PostsTags = new TableQuery(tag => new PostsTags(tag))
  def get = PostsTags
}
/*
  /** Foreign key referencing MemberCategory (database name group_rel_typeid_fkey) */
  lazy val memberCategoryFk = foreignKey("group_rel_typeid_fkey", typeid, MemberCategory.MemberCategory)(r => r.catId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UserGroups (database name group_rel_groupid_fkey) */
  lazy val userGroupsFk = foreignKey("group_rel_groupid_fkey", groupid, UserGroups.UserGroups)(r => r.groupid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)


 */