package school.model.users

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
import java.time.LocalDateTime

trait UnsafeIdentifiable {
  def userPass    :Option[String] 
  def registerDate:LocalDateTime
}

trait Identifiable { 
  def userId      :Long             
  def userLogin   :String
  def userMail    :Option[String] 
  def enabled     :Int
  def rememberMe  :Option[Int]  
}

trait Detailed {
  def phone       :Option[String] 
  def name        :Option[String] 
  def surname     :Option[String]  
  def middlename  :Option[String] 
  def birthDay    :Option[LocalDateTime]
  def nick        :String             
  def avatarFileId:Option[Long] 
  def sex         :Option[Int] 
  def aboutMe     :Option[String]  
}

case class User (
                 userLogin   :String,
                 userPass    :Option[String],
                 nick        :String,
                 userId      :Long = 0L,
                 userMail    :Option[String] = None,
                 enabled     :Int = 1,
                 rememberMe  :Option[Int] = Some(0),
                 phone       :Option[String] = None,
                 name        :Option[String] = None,
                 surname     :Option[String] = None,
                 middlename  :Option[String] = None,
                 birthDay    :Option[LocalDateTime] = None,
                 avatarFileId:Option[Long] = None,
                 sex         :Option[Int] = None,
                 aboutMe     :Option[String] = None,
                 registerDate:LocalDateTime = LocalDateTime.now)
                 extends UnsafeIdentifiable 
                 with Identifiable
                 with Detailed

/**
 * Option[T] - nullable fields
 * T - notNull fields
 */
class Users (tag:Tag) extends Table[User](tag, "users"){
  def userId       = column[Long]("userid", O.PrimaryKey, O.AutoInc)
  def userLogin    = column[String]("userlogin")
  def userPass     = column[Option[String]]("userpass")
  def userMail     = column[Option[String]]("usermail")
  def enabled      = column[Int]("enabled", O.Default(1))
  def rememberMe   = column[Option[Int]]("rememberme", O.Default(Some(0)))
  def phone        = column[Option[String]]("phone")
  def name         = column[Option[String]]("name")
  def surname      = column[Option[String]]("surname")
  def middlename   = column[Option[String]]("middlename")
  def birthDay     = column[Option[LocalDateTime]]("birthday")
  def nick         = column[String]("nick")
  def avatarFileId = column[Option[Long]]("avatarfileid") 
  def sex          = column[Option[Int]]("sex")
  def aboutMe      = column[Option[String]]("aboutme")  
  def registerDate = column[LocalDateTime]("registerdate", O.Default(LocalDateTime.now))
  def * = (userLogin, userPass, nick, userId, userMail ,  enabled, rememberMe,
           phone,  name,      surname,  middlename, birthDay,     avatarFileId,
           sex,    aboutMe, registerDate) <> (
    (User.apply _).tupled, User.unapply)
  //optional foreign key definition
  //def supplier = foreignKey("SUP_FK", supID, suppliers)(_.id) 

  /** Uniqueness Index over (usermail) (database name unique_usermail) */
  val index1 = index("unique_usermail", userMail, unique=true)
  /** Uniqueness Index over (userlogin) (database name users_userlogin_key) */
  val index2 = index("users_userlogin_key", userLogin, unique=true)
}

object Users{
  /** Collection-like TableQuery object for table Users */
  lazy val Users = new TableQuery(tag => new Users(tag))
  def get = Users
}
//  @Transient
//  String lang;
//  @Transient
//  String ip;
//  @Transient
//  Integer regmode = 1;

case class NoneUser(
                 override val userId      :Long = -1L,             
                 override val userLogin   :String = "",
                 override val userPass    :Option[String] = None, 
                 override val userMail    :Option[String] = None, 
                 override val enabled     :Int = 0,
                 override val rememberMe  :Option[Int] = None,  
                 override val phone       :Option[String] = None, 
                 override val name        :Option[String] = None, 
                 override val surname     :Option[String] = None,  
                 override val middlename  :Option[String] = None, 
                 override val birthDay    :Option[LocalDateTime] = None, 
                 override val nick        :String = "",             
                 override val avatarFileId:Option[Long] = None, 
                 override val sex         :Option[Int] = None, 
                 override val aboutMe     :Option[String] = None,  
                 override val registerDate:LocalDateTime = LocalDateTime.now)
                 extends UnsafeIdentifiable 
                 with Identifiable
                 with Detailed
                 
  /** Entity class storing rows of table UserRegistrationLog
   *  @param regid Database column regid SqlType(bigserial), AutoInc, PrimaryKey
   *  @param hashtag Database column hashtag SqlType(varchar), Length(200,true)
   *  @param expiredate Database column expiredate SqlType(timestamp)
   *  @param userid Database column userid SqlType(int8)
   *  @param enabled Database column enabled SqlType(int2), Default(0) */
  case class UserRegistrationLogRow(
      hashtag: String,
      expiredate: LocalDateTime, 
      userid: Long,
      regid: Long = 0L,
      enabled: Short = 0)
  /** Table description of table user_registration_log. Objects of this class serve as prototypes for rows in queries. */
  class UserRegistrationLog(_tableTag: Tag) extends Table[UserRegistrationLogRow](_tableTag, "user_registration_log") {
    def * = (hashtag, expiredate, userid, regid, enabled) <> (UserRegistrationLogRow.tupled, UserRegistrationLogRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(hashtag), Rep.Some(expiredate), Rep.Some(userid), Rep.Some(regid), Rep.Some(enabled)).shaped.<>({r=>import r._; _1.map(_=> UserRegistrationLogRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column regid SqlType(bigserial), AutoInc, PrimaryKey */
    val regid: Rep[Long] = column[Long]("regid", O.AutoInc, O.PrimaryKey)
    /** Database column hashtag SqlType(varchar), Length(200,true) */
    val hashtag: Rep[String] = column[String]("hashtag", O.Length(200,varying=true))
    /** Database column expiredate SqlType(timestamp) */
    val expiredate: Rep[java.time.LocalDateTime] = column[java.time.LocalDateTime]("expiredate")
    /** Database column userid SqlType(int8) */
    val userid: Rep[Long] = column[Long]("userid")
    /** Database column enabled SqlType(int2), Default(0) */
    val enabled: Rep[Short] = column[Short]("enabled", O.Default(0))

    /** GetResult implicit for fetching UserRegistrationLogRow objects using plain SQL queries */
    implicit def GetResultUserRegistrationLogRow(implicit e0: GR[Long], e1: GR[String], e2: GR[java.time.LocalDateTime], e3: GR[Short]): GR[UserRegistrationLogRow] = GR{
      prs => import prs._
      UserRegistrationLogRow.tupled((<<[String], <<[java.time.LocalDateTime], <<[Long], <<[Long], <<[Short]))
    }

}

object UserRegistrationLog{
  /** Collection-like TableQuery object for table UserRegistrationLog */
  lazy val UserRegistrationLog = new TableQuery(tag => new UserRegistrationLog(tag))
  def get = UserRegistrationLog
}