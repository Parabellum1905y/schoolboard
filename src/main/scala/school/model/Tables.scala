package school.model

// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
import db.{PostgresDriverPgExt => profile}
object Tables extends {
  val profile = db.PostgresDriverPgExt
} with Tables
/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  import profile.api._
  import slick.model.ForeignKeyAction
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

}






