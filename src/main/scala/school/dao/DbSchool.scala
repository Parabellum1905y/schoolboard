package school.dao

import db.Crypt
import db.dbpoolconf.Datasource

/**
  * Created by luger on 26.02.16.
  */
trait DbSchool {
  implicit val db = Datasource("school").database
  val crypt = Crypt
  def uuid = java.util.UUID.randomUUID.toString
}

object DbSchool extends DbSchool