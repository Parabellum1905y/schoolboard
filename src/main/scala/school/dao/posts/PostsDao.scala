package school.dao.posts

/**
  * Created by luger on 10.03.16.
  */

import java.time.LocalDateTime

import db.PostgresDriverPgExt.api._
import school.dao.DbSchool
import school.model.posts.{PostsTagsRow, PostsTags, Posts, PostsRow}
import school.model.tags.{TagsRow, Tags}
import school.model.users.{Users, User}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * managing posts in posts database
  */
object PostsDao extends DbSchool with xitrum.Log {
  val posts = Posts.get
  val postsTags = PostsTags.get
  val tags = Tags.get
  val users = Users.get

  type PostLine = Option[PostsRow]
  type TagsLine = Option[PostsRow]
  type postTuple = (PostLine, Option[TagsLine])

  def getPostById (id:Long):Future[Option[PostsRow]]= {
    try db.run(posts.filter ({ x => x.postid===id }).result.headOption)
    catch {
      case ex:Exception => {
        log error ("ex:", ex)
        Future[Option[PostsRow]] {
          None
        }
      }
    }
    //    finally db.close()
  }

  def getPostByIdWithTagsUser (id:Long):Future[Seq[(Option[PostsRow], Option[Option[TagsRow]], User)]]= {
    val query = for {
      ((p, pt), t) <- posts.filter({x => x.postid === id}).
        joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
        joinLeft (tags).on(_._2.map(_.tagid) === _.tagid)
      user <- users.filter(_.userId === p.userid)
    } yield (p.?, t.map(_.?), user)
    try db.run(query.result)
    catch {
      case ex:Exception => {
        log error("ex:", ex)
        Future[Seq[(Option[PostsRow], Option[Option[TagsRow]], User)]] {
          Seq((None, None, User("", Some(""), "", 0L)))
        }
      }
    }
    //    finally db.close()
  }

  def getLast (userId:Long, mode:String, count:Int = 16):Future[Seq[(Option[PostsRow], Option[Option[TagsRow]])]]={
    mode match {
      case "own"=>
        val query = for {
          ((p, pt), t) <- posts.filter({x => x.userid === userId}).sortBy({x=>x.createdate.desc}).take(count).
                          joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
                          joinLeft (tags).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.map(_.?))
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[Option[TagsRow]])]] {
              Seq((None, None))
            }
          }
        }
      case _ =>
        val query = for {
          ((p, pt), t) <- posts.sortBy({x=>x.createdate.desc}).take(count).
            joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
            joinLeft (tags).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.map(_.?))
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[Option[TagsRow]])]] {
              Seq((None, None))
            }
          }
        }
    }
  }

  def getLastForTag (userId:Long, mode:String, tagId:Long):Future[Seq[(Option[PostsRow], Option[TagsRow])]]={
    mode match {
      case "own"=>
        val query = for {
          ((p, pt), t) <- posts.filter({x => x.userid === userId}).sortBy({x=>x.createdate.desc}).
            joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
            join (tags filter(_.tagid === tagId)).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.?)
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[TagsRow])]] {
              Seq((None, None))
            }
          }
        }
      case _ =>
        val query = for {
          ((p, pt), t) <- posts.sortBy({x=>x.createdate.desc}).
            joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
            join (tags filter(_.tagid === tagId)).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.?)
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[TagsRow])]] {
              Seq((None, None))
            }
          }
        }
    }
  }

  def getLastForTagText (userId:Long, mode:String, tagText:String):Future[Seq[(Option[PostsRow], Option[TagsRow])]]={
    mode match {
      case "own"=>
        val query = for {
          ((p, pt), t) <- posts.filter({x => x.userid === userId}).sortBy({x=>x.createdate.desc}).
            joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
            join (tags.filter(_.tagtext.toUpperCase like "%"+tagText.toUpperCase+"%")).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.?)
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[TagsRow])]] {
              Seq((None, None))
            }
          }
        }
      case _ =>
        val query = for {
          ((p, pt), t) <- posts.sortBy({x=>x.createdate.desc}).
            joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
            join (tags.filter(_.tagtext.toUpperCase like "%"+tagText.toUpperCase+"%")).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.?)
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[TagsRow])]] {
              Seq((None, None))
            }
          }
        }
    }
  }
  /*def getNext (userId:Long, mode:String, lastDate:LocalDateTime, count:Int = 16):Future[Seq[PostsRow]]={

  }*/

  def getPrev (userId:Long, mode:String, firstDate:LocalDateTime, count:Int = 16):Future[Seq[(Option[PostsRow], Option[Option[TagsRow]])]]={
    mode match {
      case "own"=>
        val query = for {
          ((p, pt), t) <- posts.filter({x => x.userid === userId && x.createdate<firstDate}).sortBy({x=>x.createdate.desc}).take(count).
            joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
            joinLeft (tags).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.map(_.?))
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[Option[TagsRow]])]] {
              Seq((None, None))
            }
          }
        }
      case _ =>
        val query = for {
          ((p, pt), t) <- posts.filter({x =>x.createdate<firstDate}).sortBy({x=>x.createdate.desc}).take(count).
            joinLeft (postsTags).on((x, y)=>x.postid === y.postid).
            joinLeft (tags).on(_._2.map(_.tagid) === _.tagid)
        } yield (p.?, t.map(_.?))
        //val query = posts filter({x => x.userid === userId}) sortBy({x=>x.createdate.desc}) take(count)
        try db.run(query.result)
        catch{
          case ex:Exception => {
            log error("ex:", ex)
            Future[Seq[(Option[PostsRow], Option[Option[TagsRow]])]] {
              Seq((None, None))
            }
          }
        }
    }
  }

  def update (post:PostsRow) = {
  }

  def create (post:PostsRow):Future[Long] = {
    val q = (posts returning posts.map ( _.postid )) += post
    db.run(q)
  }

  def createRet (post:PostsRow):Future[Option[PostsRow]] = {
    val q = (posts returning posts.map ( _.postid )) += post
    for {
      id:Long    <- db.run(q)
      createdPost<- Future {
        if (id>0) Some(post.copy(postid = id)) else None
      }
    }yield createdPost
  }

  def createTagsQuery (tagsText:Seq[String]) = {
    val query = tags returning tags.map(_.tagid) ++= tagsText.map{text=> TagsRow(tagid = 0L, tagtext = text)}
    query
  }

  /**
    * create new post, tags, and post-tags relation
    * NOTE:tags equation in Upper Case
    *
    * @param post
    * @param tagsRows
    * @return
    */
  def createWithTags (post:PostsRow, tagsRows:Seq[String]):Future[Long] = {
    val filteredTag  = tags.filter(_.tagtext.toUpperCase inSetBind tagsRows.map(_.toUpperCase)).result
    val allIns = (for {
      ftags: Seq[TagsRow]    <- filteredTag // found tags
      newTags = tagsRows.filterNot{elem => ftags map (_.tagtext.toUpperCase) contains(elem.toUpperCase)}
      createdTags: Seq[Long] <- createTagsQuery(newTags)
      newPostId: Long <- posts returning posts.map(_.postid) += post
      newPostTagCount <- postsTags ++= (ftags.map(_.tagid) ++ createdTags) map {tagid=>PostsTagsRow(newPostId, tagid)}
    } yield newPostId).transactionally
    db.run(allIns)
  }

  def createWithTagsRet (post:PostsRow, tagsRows:Seq[String]):Future[Option[PostsRow]] = {
    for {
      newPostId <- createWithTags(post, tagsRows)
      createdPost<- Future {
        if (newPostId>0) Some(post.copy(postid = newPostId)) else None
      }
    } yield createdPost
  }
  /*
  def createRegistryLog (post:PostsRow):Future[Long] = {
    val regLog = new UserRegistrationLogRow(crypt.sha1(file.userLogin)+uuid, LocalDateTime.now.plusHours(24), file.userId)
    val query = (registrations returning registrations.map(_.regid)) += regLog
    db run query
  }

  def createRegistryLogHashRet (post:PostsRow):Future[String] = {
    val hash = crypt.sha1(file.userLogin)+uuid
    val regLog = new UserRegistrationLogRow(hash, LocalDateTime.now.plusHours(24), file.userId)
    val query = (registrations returning registrations.map(_.regid)) += regLog
    for {
      regid <- db run query
      h     <- Future {if (regid>0) hash else ""}
    } yield h
  }

  /**
    * create file, create log row for file registration, return new PostsRow object and reg hash
    *
    * @param file file to create
    * @return
    */
  def createRetAndLog (post:PostsRow):Future[(Option[PostsRow], String)] = {
    createRet(file).flatMap {
      case userOpt@None => Future {
        (userOpt, "")
      }
      case some => createRegistryLogHashRet(some.get).flatMap({
        hash=>Future{(some, hash)}
      })
    }
  }

  /**
    * create file, create log row for file registration, return new PostsRow Id and reg hash
    *
    * @param file file to create
    * @return
    */
  def createAndLog (post:PostsRow):Future[(Long, String)] = {
    /*createRet(file).flatMap(opt=>opt.fold(Future{(-1L, "")}) {
      u=>createRegistryLogHashRet(u).flatMap {
        hash => Future {
          (u.userId, hash)
        }
      }
    })*///valid CODE , but i want to try for-comprehensions
    for {
      retUser <- createRet(file)
      ret     <- retUser.fold(Future{(-1L, "")}) {u=>
        createRegistryLogHashRet(u).flatMap {
          hash => Future {(u.userId, hash)}
        }
      }
    } yield (ret)
  }

  /**
    * full activate file by hash in reg log
    *
    * @param regHash
    */
  def activateUser (regHash:String):Future[Int] = {
    val filteredReg = registrations filter(_.hashtag === regHash)
    val userIdQuery = filteredReg  sortBy(p=>p.expiredate.desc) map (reg => reg.userid)
    val allUpd = (for {
      _          <- filteredReg map (reg => reg.enabled) update (1)
      result:Int <- posts.filter(_.fileid in userIdQuery) map (us => (us.registerDate, us.enabled)) update (LocalDateTime.now, 1)
    } yield result).transactionally
    db.run(allUpd)
  }
  */
}
