package school.dao.usersDAO

import db.PostgresDriverPgExt.api._
import school.dao.DbSchool
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import java.time.LocalDateTime
import school.model.users.{UserRegistrationLogRow, User, Users, UserRegistrationLog}

/**
 * managing users in users database
 */
object UserDao extends DbSchool with xitrum.Log {
  val users = TableQuery[Users]
  val registrations = UserRegistrationLog.get

  def getUserById (id:Long):Future[Option[User]]= {
    try db.run(users.filter ({ x => x.userId===id }).result.headOption)
    catch {
      case ex:Exception => {
        log error ("ex:", ex)
        Future[Option[User]] {
          None
        }
      }
    }
//    finally db.close()
  }
  
  def getUserByLogin (login:String):Future[Option[User]]= {
    try db.run(users.filter ({ x => x.userLogin===login }).result.headOption)
    catch {
      case ex:Exception => {
        log error ("ex:", ex)
        Future[Option[User]] {
          None
        }
      }
    }
  }

  def getUserByLoginSafe (login:String):Future[Option[User]] = getUserByLogin (login).map  (x => x match {
    case Some(x)=>Some(x copy(userPass = None, registerDate = LocalDateTime.now))
    case _=>None
  })
  
  def getUserByLoginSafe (login:String, pwd:String):Future[Option[User]] = getUserByLogin (login, pwd).map  (x => x match {
    case Some(x)=>Some(x copy(userPass = None, registerDate = LocalDateTime.now))
    case _=>None
  })

  def getUserByIdSafe (id:Long):Future[Option[User]] = getUserById (id).map  (x => x match {
    case Some(x)=>Some(x copy(userPass = None, registerDate = LocalDateTime.now))
    case _=>None
  })

  def getUserByLogin (login:String, pwd:String):Future[Option[User]]= {
    try db.run(users.filter ( x => x.userLogin===login && x.userPass === crypt.sha1(pwd)).result.headOption)
    catch {
      case ex:Exception => {
        log error ("ex:", ex)
        Future[Option[User]] {
          None
        }
      }
    }
  }

  def update (user:User):Future[Int] = {
    val query = users.filter(_.userId === user.userId).
              map(u=>(u.avatarFileId, u.aboutMe, u.name, u.surname, u.birthDay))
    db.run(query.update((user.avatarFileId, user.aboutMe, user.name, user.surname, user.birthDay)))
  }
  
  def create (user:User):Future[Long] = {
    val q = (users returning users.map ( _.userId )) += user.copy(userPass = Some(crypt.sha1(user.userPass.get)))
    db.run(q) 
  }

  def createRet (user:User):Future[Option[User]] = {
    val q = (users returning users.map ( _.userId )) += user.copy(userPass = Some(crypt.sha1(user.userPass.get)))
    for {
      id:Long    <- db.run(q)
      createdUser<- Future {
        if (id>0) Some(user.copy(userId = id)) else None
      }
    }yield createdUser
  }

  def createRegistryLog (user:User):Future[Long] = {
    val regLog = new UserRegistrationLogRow(crypt.sha1(user.userLogin)+uuid, LocalDateTime.now.plusHours(24), user.userId)
    val query = (registrations returning registrations.map(_.regid)) += regLog
    db run query
  }

  def createRegistryLogHashRet (user:User):Future[String] = {
    val hash = crypt.sha1(user.userLogin)+uuid
    val regLog = new UserRegistrationLogRow(hash, LocalDateTime.now.plusHours(24), user.userId)
    val query = (registrations returning registrations.map(_.regid)) += regLog
    for {
      regid <- db run query
      h     <- Future {if (regid>0) hash else ""}
    } yield h
  }

  /**
    * create user, create log row for user registration, return new User object and reg hash
    *
    * @param user user to create
    * @return
    */
  def createRetAndLog (user:User):Future[(Option[User], String)] = {
    createRet(user).flatMap {
      case userOpt@None => Future {
        (userOpt, "")
      }
      case some => createRegistryLogHashRet(some.get).flatMap({
        hash=>Future{(some, hash)}
      })
    }
  }

  /**
    * create user, create log row for user registration, return new User Id and reg hash
    *
    * @param user user to create
    * @return
    */
  def createAndLog (user:User):Future[(Long, String)] = {
    /*createRet(user).flatMap(opt=>opt.fold(Future{(-1L, "")}) {
      u=>createRegistryLogHashRet(u).flatMap {
        hash => Future {
          (u.userId, hash)
        }
      }
    })*///valid CODE , but i want to try for-comprehensions
    for {
      retUser <- createRet(user)
      ret     <- retUser.fold(Future{(-1L, "")}) {u=>
                  createRegistryLogHashRet(u).flatMap {
                      hash => Future {(u.userId, hash)}
                  }
      }
    } yield (ret)
  }

  /**
    * full activate user by hash in reg log
    *
    * @param regHash
    */
  def activateUser (regHash:String):Future[Int] = {
    val filteredReg = registrations filter(_.hashtag === regHash)
    val userIdQuery = filteredReg  sortBy(p=>p.expiredate.desc) map (reg => reg.userid)
    val allUpd = (for {
      _          <- filteredReg map (reg => reg.enabled) update (1)
      result:Int <- users.filter(_.userId in userIdQuery) map (us => (us.registerDate, us.enabled)) update (LocalDateTime.now, 1)
    } yield result).transactionally
    db.run(allUpd)
  }
}
