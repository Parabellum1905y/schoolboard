function afterNewAvatarCallback(data){
    $(".loader-img").remove();
    if (data.status == "success"){
        $("#userphoto").html ("<img style=\"position:absolute;top:0;left:0\" src=\""+xitrum.withBaseUrl("/image/"+data.data)+"\"/>");
        $("input#changeAvatarFileId").val (data.data);
    }else $("#userphoto").html ("<i class=\"fa fa-user\"></i>");
}

function afterChangeProfileCallback(data){
    $(".loader-img").remove();
    if (data.status == "success"){
        location.href = xitrum.withBaseUrl("");
    }
}

function getProfile(url, obj){
	    //$('.right-sidebar.full-text.profile').show('slow');
		loadDataAsync (xitrum.withBaseUrl(url),
		                    {type:"GET",his:false, p:null, setParams:false},
				{}, null,
				function(data){
					if (!isEmpty (data)){
						$(obj).html(data);
						//$(".profile-page").hide();
						//setRightSideBarScroll(".profile-page");
						//$("#profile-view").show("slow");
				    	//$(".profile-open-page:first").addClass ("opened");
					}
				});
//	    openRightSidebar ();
}

function after400Error(xhr){
    $(".loader-img").remove();
    if (xhr.responseText.startsWith("Request content body is too big.")){
        alert ("Файл слишком большой");
    }
}

$(document).ready(function(){
    $("#openProfile").click (function (){
    });

    $("#open-profile").click (function (e){
        e.preventDefault();
        $(".profile#settings").hide("slow");
        $(".profile#profile").show("slow");
    });

    $("#open-profile-settings").click (function (e){
        e.preventDefault();
        $(".profile#profile").hide("slow");
        $(".profile#settings").show("slow");
    });

    $("#newavatar").change(function(e){
        $("#changePhoto").submit();
        //newPostCallback(e, $("#changePhoto"), afterNewAvatarCallback);
    });

    $("#changePhoto").submit (function(e){
        newUploadCallback(e, this, afterNewAvatarCallback, after400Error);
    });

    $("#changeProfile").submit (function(e){
        newUploadCallback(e, this, afterChangeProfileCallback, after400Error);
    });

    $("body").on ("click", ".link2.author", function () {
        $(".rightsidebar").hide ();
        $("#anotherProfileBlock").show("slow");
    	getProfile ($(this).data ("url"), $("#anotherProfileBlock"));
    });
});