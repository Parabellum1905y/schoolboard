var Config = {
    root : ""
}

if (typeof String.prototype.startsWith != 'function') {
  // see below for better implementation!
	  String.prototype.startsWith = function (str){
	    return this.indexOf(str) == 0;
	  };
} 

(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

function mylocation (path){
	var root = location.protocol + '//' + location.host;
	if (path.startsWith (root)) return path;
	rootLoc = Config.root;
	root = root.length>0 && root[root.length -1] == '/'?root.substring (0, root.length-1):root;
	rootLoc= root + rootLoc;
	if (path.length>1 && path[0] != "/" && rootLoc.length>0 && rootLoc[rootLoc.length-1] != "/")rootLoc= rootLoc+"/";
	if (path.length>1 && path[0] == "/" && rootLoc.length>0 && rootLoc[rootLoc.length-1] == "/")
		rootLoc= rootLoc.substring (0, rootLoc.length-1);
	return rootLoc + path;
}

function str2date(s) {
    if (/^\d{2}\.\d{2}\.\d{4}$/.test(s)) {
    var dateParts = s.split('.');
        return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
    }
    return null;
}

function isDate(value) {
	
	if (!(/^\d{2}\.\d{2}\.\d{4}$/.test(value))) {
	return false;
}

var dd1, mm, yy, tt;
tt=value.split('.');
	dd1=parseInt(tt[0], 10);
	mm=parseInt(tt[1], 10);
	yy=parseInt(tt[2], 10);
	
	if (yy > 3000 || yy < 1900 || mm > 12 || mm < 0) {
		return false;
	}
	
	var n;
	if (mm == 2){
		if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0){
			n = 29;
		}else{
			n = 28;
		}
	} else {
		if (mm == 4 || mm == 6 || mm == 9 || mm == 11){
			n = 30;
		} else {
			n = 31;
		}
	}
		
	if (dd1 > n || dd1 < 1){
		return false;
	}
	
	return true;
}

function isTime(value) {
	var v = value.split(':');
if (v[0].length < 2) {
	value = '0' + value;
}
return (/^(([0-1][0-9]|[2][0-3])(:([0-5][0-9])){1,2})|(([0-1][0-9]|[2][0-3])(:([0-5][0-9])){1,2}(:([0-5][0-9])){1,2})$/.test(value));
}

function isDouble(value){
	if (value == '-0') return false;
	return /(^[-+]?\d+((\.|\,)\d+)?([eE]{1}[-+]?\d+((\.|\,)\d+)?)?)$/.test (value) || isInt (value);
}

function getDouble(value) {
	if (isBlank(value)) {
		return '';
}
return (value+"").replace(',', '.');
}

function isInt(value) {
	if (value == '-0') return false;
	return /^\-?[1-9]+[0-9]*$/.test(value) || (value.length == 1 && value == 0); 
}

function isInteger(value) {
	return isInt (value);
}

function isNumber(value) {
	return (/^\d+$/.test(value));
}

function returnDay(ddate){
	var dd,mm,yy,tt;
	tt=ddate.split('.');
	dd=parseInt(tt[0],10);
	mm=parseInt(tt[1],10);
	yy=parseInt(tt[2],10);
	
	var n;
	if (mm == 2) {
		if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0){
			n = 29;
		} else {
			n = 28;
		}
	} else {
		if (mm == 4 || mm == 6 || mm == 9 || mm == 11){
			n = 30;
		} else {
			n = 31;
		}
	}
	if (dd > n || dd < 1){
		return n;
	} else  return dd;
}

function isEmpty(str) {
    return str == null 
       || str.length == 0;
}

function isBlank(str) {
	return str == null 
    	|| str.length == 0 
    	|| /^\s*$/.test(str);
}

function isDatePeriod(date1, date2) {
	if (isDate(date1) && isDate(date2)) {
		if (str2date(date1) <= str2date(date2)) {
			return true;
		}
	}
	return false;
}

function filter(selector, query, query1, column, type) {
	query = $.trim(query); 
	query = query.replace(/ /gi, '|'); 
    query1 = $.trim(query1);

    var a = $(selector);

    for (var i = 0; i < a.length; i++) {
        var b = $(a[i]).children();
        var c = $.trim($(b[column]).text());
        if (type == 'date') {
            if (str2date(query) != null && str2date(query1) != null) {
                str2date(query) <= str2date(c) && str2date(query1) >= str2date(c)  ? $(a[i]).show() : $(a[i]).hide();
            } else if (str2date(query) == null) {
                str2date(query1) >= str2date(c) ? $(a[i]).show() : $(a[i]).hide();
            } else if (str2date(query1) == null) {
                str2date(query) <= str2date(c) ? $(a[i]).show() : $(a[i]).hide();
            }
            if (str2date(query) == null && str2date(query1) == null) {
                $(a[i]).show();
            }
        }

        if (type != 'date') {
		    ($(b[column]).text().search(new RegExp(query, "i")) < 0) ? $(a[i]).hide() : $(a[i]).show();
		}
	}
}

function setSelect(id, value) {
	$('#' + id + ' option').each(function() {
	if ($(this).text() == value) {
		$(this).attr('selected', 'selected');
			return;
		}
	});
}

function isEmail(value) {
	
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,6})+$/;
	if (filter.test(value)) {
		return true;
	}
	return false;
}

function getInteger(value) {
	if (isInteger(value)) {
		return parseInt(value, 10);
	}
	return null;
}

function myRound(value, places) { 
	/*var multiplier = Math.pow(10, places); 
return (Math.round(value * multiplier) / multiplier);*/
    var a = new BigNumber (value, places, BigNumber.ROUND_HALF_UP);
    return a.toString().replace(',', '.');
} 

function isLongWord(str,n){	
	var j=0;
	for (var i=0;i<=str.length;i++)		
		if (str[i]==' ' || i==str.length || str[i]=='\n'){ 		
			if (i-j>n) {
				return false;
				break;
			}
			else j=i;
		}	
	return true;
}

function getFirst(value) {
	value = value.replace(',', '.');
var a = value.split('.');
	return a[0];
}

function getSecond(value) {
	value = value.replace(',', '.');
var a = value.split('.');
if (a.length == 2) {
	return a[1];
}
return '';
}

function getString(key, params) {
	var keis = key.split(",");
var  res = "";
var j = 0;
for (var i = 0; i < keis.length; i++) {
	if (keis[i] == "?") {
		res = isEmpty(res) ? params[j] : res + " " + params[j];
		j++;
	} else {
		res = isEmpty(res) ? jQuery.i18n.prop(keis[i]) : res + " " + jQuery.i18n.prop(keis[i]);
		}
	}
	
	return res;
}

function safe_tags(str) {
    return str.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') ;
}

function trim(str, chars) { 
	return ltrim(rtrim(str, chars), chars); 
} 
 
function ltrim(str, chars) { 
	chars = chars || "\\s"; 
return str.replace(new RegExp("^[" + chars + "]+", "g"), ""); 
} 
 
function rtrim(str, chars) { 
	chars = chars || "\\s"; 
return str.replace(new RegExp("[" + chars + "]+$", "g"), ""); 
}

function del_spaces(str){
	if (str){
	    str = str.replace(/\s/g, '');
        return str;
    }else return "";
}


function loadData (path, type, elementToLoad){
	setTimeZoneInCookie ();
	$.ajax({
		url: path,
		"type": type,
	    async:false,
	    success: function(data) {
            if (jqXHR.getResponseHeader('OWN') == 'REDIRECT'){
                location.href = mylocation (jqXHR.getResponseHeader('REDIR_PATH'));
                return;
            }
            $("#"+elementToLoad).html (data);
        },
    	statusCode: {
            502: function() {
              location.href = mylocation ("");
            },
            500: function() {
                  location.href = mylocation ("");
            },
            403: function() {
                  location.href = mylocation ("");
            },
            401: function() {
                  location.href = mylocation ("login");
                }
		  }
	});
}

function loadDataAsync (path, type, postData, elementToLoad, onSuccessResultHandler, onErrorHandler, on400Handler){
	setTimeZoneInCookie ();
	console.log (type.file+";"+(type.file?false:true));
	$.ajax({
		url: path,
		"type": type.type,
	    data : postData,
	    processData: type.file?false:true,
        contentType: type.file?false:'application/x-www-form-urlencoded; charset=UTF-8',
	    async:true,
	    success: function(data, textStatus, jqXHR) {
		if (type.type == 'GET' && type.his && supports_history_api()){
			var paramsStr = "";
			if (type.setParams){
				for (var index in postData)paramsStr += "&pp_"+index+"="+encodeURIComponent(postData[index]).replace(/\s/g, '&space;').replace(/#/g, '&hash;');
			}
			history.pushState(null, null, "?p="+type.p+(type.setParams?paramsStr:""));
		}
		if (jqXHR.getResponseHeader('OWN') == 'REDIRECT'){
			location.href = mylocation (jqXHR.getResponseHeader('REDIR_PATH'));
			return;
		}
		if (elementToLoad){
			$("#"+elementToLoad).html (data);
		}
		if (onSuccessResultHandler){
			onSuccessResultHandler (data);
		}
	},
	error: function(request, status, error){
        try {
        	console.log(status);
        	console.log(error);
            console.log(request.getAllResponseHeaders());
            if (onErrorHandler)onErrorHandler(data);
        }
        catch (err) { }
    },
    statusCode: {
		303:function() {
	    	alert ("Ooooo");
	    },
		307:function() {
			alert ("Aaaaa");
	    },
	    502: function() {
	    	location.href = mylocation ("");
	    },
	    500: function() {
	    	location.href = mylocation ("");
	    },
	    403: function() {
		    location.href = mylocation ("");
	    },
        400: function(request, status, error){
                     try {
                     	console.log(status);
                     	console.log(error);
                     	console.log(request);
                     	if (request.responseText.startsWith("alert(\"Session expired."))location.href = mylocation ("/");
                        if (on400Handler)on400Handler(request);
                        else location.href = mylocation ("/");
                     }
                     catch (err) { }
             },
	    401: function() {
	    	location.href = mylocation ("login");
		    }
		  }

	});
}

window.addEventListener('popstate', function(e){
  if (e.state)location.href = e.state.path;
}, false);
/**
 * checking support for history api
 * @returns {Boolean}
 */
function supports_history_api() {
	return !!(window.history && history.pushState);
}

function setTimeZoneInCookie() {
    var _myDate = new Date();
    var _offset = _myDate.getTimezoneOffset();
    document.cookie = "USER_TIMEZONE=" + _offset; //Cookie name with value
}

function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
			"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function getFormattedDate (date, withoutTime){
    var day = date.getDate()+"";
    var month = (date.getMonth() + 1)+"";
    var year = date.getFullYear().toString().slice(2)+"";
    var seconds = date.getSeconds()+"";
    var minutes = date.getMinutes()+"";
    var hour = date.getHours()+"";
    day = day.length == 1?"0"+day:day;
    month = month.length == 1?"0"+month:month;
    year = year.length == 1?"0"+year:year;
    seconds = seconds.length == 1?"0"+seconds:seconds;
    minutes = minutes.length == 1?"0"+minutes:minutes;
    hour = hour.length == 1?"0"+hour:hour;
    if(withoutTime)
        return day + '.' + month + '.' + date.getFullYear();
    else
        return day + '.' + month + '.' + year+" "+hour+":"+minutes+":"+seconds;
}


function stringify(obj) {         
    if ("JSON" in window) {
      return JSON.stringify(obj);
    }

    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"' + obj + '"';

        return String(obj);
    } else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);

        for (n in obj) {
            v = obj[n];
            t = typeof(v);
            if (obj.hasOwnProperty(n)) {
                if (t == "string") {
                    v = '\"' + v + '\"';
                } else if (t == "object" && v !== null){
                    v = jQuery.stringify(v);
                }

                json.push((arr ? "" : '\"' + n + '\":') + String(v));
            }
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
}

function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function templater (templates, text){
	var textReplaced = text;
	for (key in templates){
		replaced = templates[key];
		textReplaced = textReplaced?textReplaced.replace (new RegExp(escapeRegExp("\@\{"+key+"\}"), 'g'), replaced):"";
	}
	return textReplaced;
}


function println (message, object){
	if (message && jQuery.i18n){
		 var text = jQuery.i18n.prop(message);
		 if (text){
			 if (object){
				 for (key in object){
					 replaced = object[key];
					 text = text.replace (new RegExp(escapeRegExp("\{"+key+"\}"), 'g'), replaced);
			     }
		     }
	    	 return text;
    	 }
    }
    return "";
}

function getNumEnding(iNumber, aEndings){
    var sEnding, i;
    iNumber = iNumber % 100;
    if (iNumber>=11 && iNumber<=19) {
        sEnding=aEndings[2];
    }else {
        i = iNumber % 10;
        switch (i){
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}

function openLocByHash (){
	var locationHash = window.location.hash;
	
}

String.prototype.splice = function( idx, rem, s ) {
    return (this.slice(0,idx) + s + this.slice(idx + Math.abs(rem)));
};
