xitrum.ajaxLoadingImg = xitrum.withBaseUrl("/images/ajax-loader_3.gif");

var minePosts = false;

function getPosts(mode, type){
	    //$('.right-sidebar.full-text.profile').show('slow');
		loadDataAsync (xitrum.withBaseUrl("posts/"+type+".html"+(mode.length>0?"/"+mode:"")),
		                    {type:"GET",his:false, p:null, setParams:false},
				{}, null,
				function(data){
					if (!isEmpty (data)){
						$("div.w-row").html(data);
					}
				});
}


function getPostsForTagid(mode, tagId){
        $('.rightsidebar').hide('slow');
	    //$('.right-sidebar.full-text.profile').show('slow');
		loadDataAsync (xitrum.withBaseUrl("posts/fortagid.html"+(mode.length>0?"/"+mode:""+"?tagId="+tagId)),
		                    {type:"GET",his:false, p:null, setParams:false},
				{}, null,
				function(data){
					$("div.w-row").html(data);
				});
}

function getPostsForTagText(mode, text){
	    $('.rightsidebar').hide('slow');
		loadDataAsync (xitrum.withBaseUrl("posts/fortag.html"+(mode.length>0?"/"+mode:""+"?tag="+text)),
		                    {type:"GET",his:false, p:null, setParams:false},
				{}, null,
				function(data){
					$("div.w-row").html(data);
				});
}

var prevBlocked = false;
function getPrevPosts(mode, type, date){
        if (prevBlocked)return;
        prevBlocked = true;
		loadDataAsync (xitrum.withBaseUrl("posts/"+type+"/"+date+(mode.length>0?"/"+mode:"")),
		                    {type:"GET",his:false, p:null, setParams:false},
				{}, null,
				function(data){
				    prevBlocked = false;
					if (!isEmpty (data)){
						$("div.w-row").append(data);
					}
				});
}

function getPost(url, obj){
	    //$('.right-sidebar.full-text.profile').show('slow');
		loadDataAsync (xitrum.withBaseUrl(url),
		                    {type:"GET",his:false, p:null, setParams:false},
				{}, null,
				function(data){
					if (!isEmpty (data)){
						$(obj).html(data);
						//$(".profile-page").hide();
						//setRightSideBarScroll(".profile-page");
						//$("#profile-view").show("slow");
				    	//$(".profile-open-page:first").addClass ("opened");
					}
				});
//	    openRightSidebar ();
}

function sendFile(file, editor) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: 'POST',
            url: xitrum.withBaseUrl("/newfile"),
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                editor.summernote('insertImage', xitrum.withBaseUrl("/image/"+data.data));
            }
        });
}

function newPostCallback(e, obj, after, hideLoading){
    $(".w-form-fail").hide();
    $(".w-form-done").hide();
    e.preventDefault(); //STOP default action
    var postData = $(obj).serializeArray();
    var formURL = $(obj).attr("action");
    if (!hideLoading)$(obj).after ('<img class="loader-img" src="' + xitrum.ajaxLoadingImg + '" />');
    loadDataAsync (formURL, {type:"POST",his:false}, postData, null, after);
}

function newUploadCallback(e, obj, after, after400, hideLoading){
    $(".w-form-fail").hide();
    $(".w-form-done").hide();
    e.preventDefault(); //STOP default action
    var postData = new FormData($(obj)[0]);
    var formURL = $(obj).attr("action");
    if (!hideLoading)$(obj).after ('<img class="loader-img" src="' + xitrum.ajaxLoadingImg + '" />');
    loadDataAsync (formURL, {type:"POST",his:false,file:true}, postData, null, after, null,after400);
}

function afterLoginCallback(data){
    $(".loader-img").remove();
    if (data.status == "success"){
        location.href = xitrum.withBaseUrl("");
    }else{
        if (data.data === "INVALID_LOGIN") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин").parent().show("slow");
        if (data.data === "INVALID_PWD") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный пароль").parent().show("slow");
        if (data.data === "USER_NOT_FOUND") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин или пароль").parent().show("slow");
        if (data.data === "USER_NOT_ENABLED") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Пользователь не подтвержден").parent().show("slow");

    }
}

function afterLogoutCallback(data){
    location.href = xitrum.withBaseUrl("");
}

function afterRegCallback(data){
    $(".loader-img").remove();
    if (data.status == "success"){
        $("#reg2").parent().find(".w-form-done").find("p")
                  .html ("Поздравляем! Вы зарегистрировались у нас на сайте.<br>"+
                   "Для подтверждения регистрации Вам выслано письмо на почту!").parent().show("slow");
    }else{
        if (data.data === "INVALID_LOGIN") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин").parent().show("slow");
        if (data.data === "INVALID_PWD") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный пароль").parent().show("slow");
        if (data.data === "USER_NOT_FOUND") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин или пароль").parent().show("slow");
        if (data.data === "INVALID_PWD_CONFIRM") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверное подтверждение пароля").parent().show("slow");
        if (data.data === "NOT_EQ_PWD_CONFIRM") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Введенные пароли не идентичны").parent().show("slow");
        if (data.data === "USER_NOT_CREATED") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Ошибка :(").parent().show("slow");
        if (data.data === "ALREADY_FOUND") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Такой пользователь уже существует").parent().show("slow");
    }
}

$(document).ready(function(){
    $("#email-form-2").submit(function (e){newPostCallback(e, this, afterLoginCallback);});

    $("#email-form").submit(function (e){
        $("#message").text ($('#messagenote').summernote('code'));
        console.log ($('#messagenote').summernote('code'));
        newPostCallback(e, this, afterLoginCallback);
    });

    $("#reg2").submit(function (e){newPostCallback(e, this, afterRegCallback);});

    $("#logout").submit(function (e){newPostCallback(e, this, afterLogoutCallback, true);});

	$(document).mouseup(function (e) {
	    if(!$('#authBlock').is(e.target) && $('#authBlock').has(e.target).length === 0){
	    	$('#authBlock').hide();
	    }

	    if(!$('#regBlock').is(e.target) && $('#regBlock').has(e.target).length === 0){
	    	$('#regBlock').hide();
	    }

	    if(!$('#profileBlock').is(e.target) && $('#profileBlock').has(e.target).length === 0){
	    	$('#profileBlock').hide();
	    }

	    if(!$('#anotherProfileBlock').is(e.target) && $('#anotherProfileBlock').has(e.target).length === 0){
	    	$('#anotherProfileBlock').hide();
	    }

	    if(!$('#viewBlock').is(e.target) && $('#viewBlock').has(e.target).length === 0){
	    	$('#viewBlock').hide();
	    }
	});

    $("body").on ("click", ".link.openpost", function () {
    	$("#viewBlock").show ("slow");
    	getPost ($(this).data("url"), "#viewBlock .w-form");
    });

    $("body").on ("click", ".link2.tag", function () {
    	getPostsForTagid ($("#postsMode").val (), $(this).data("tag"));
    });

	$('#messagenote').summernote({
      height: 300,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      //focus: true                  // set focus to editable area after initializing summernote
      callbacks:{
        onImageUpload: function(files) {
            sendFile(files[0], $(this));
        }
      }
    });

    jQuery('#scrollup img').mouseover( function(){
		jQuery( this ).animate({opacity: 0.65},100);
	}).mouseout( function(){
		jQuery( this ).animate({opacity: 1},100);
	}).click( function(){
		window.scroll(0 ,0);
		return false;
	});

	jQuery(window).scroll(function(){
		if ( jQuery(document).scrollTop() > 0 ) {
			jQuery('#scrollup').fadeIn('fast');
		} else {
			jQuery('#scrollup').fadeOut('fast');
		}
	});

    $(window).scroll(function() {
        var x = $(document).height() - $(window).height()-$("#contact").height();
        if($(window).scrollTop() >= x-10 && $(window).scrollTop() >= x+10) {
            var date = $(".w-col.w-col-3.column-0-padding.bordering").last ().data("date");
            if (date){
                getPrevPosts ($("#postsMode").val (), "prev", date.split(",").join(""));
            }
        }
    });

    $("#allPosts").click (function(){
        $("#postsMode").val ("");
        getPosts ("", "last");
    });

    $("#searchTagButton").click (function (){
        getPostsForTagText ($("#postsMode").val (), $("#searchTag").val ());
    });

    $("#myPosts").click (function(){
        $("#postsMode").val ("me");
        getPosts ("me", "last");
    });

    getPosts ("", "last");
});