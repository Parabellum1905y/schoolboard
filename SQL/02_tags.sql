CREATE SEQUENCE tags_id_seq;
-- ALL TAGS
CREATE TABLE TAGS(
	tagId 			bigint default nextval('tags_id_seq'),
	tagText 		varchar(200) NOT NULL UNIQUE,
	Primary Key(tagId)
)WITH ( OIDS=FALSE );

SELECT setval('tags_id_seq', (SELECT MAX(tagId) FROM TAGS));

CREATE INDEX tags_adid_date_index
            ON tags USING btree(tagText);
-- function for checking existing tags and inserting without errors
CREATE or REPLACE function tags_ignore_duplicate_inserts() Returns Trigger
As $$
Begin
    If Exists (
        Select
            *
        From
            tags ct
        Where
            -- Assuming all three fields are primary key
            ct.tagText = NEW.tagText
    ) Then
        Return NULL;
    End If;
    Return NEW;
End;
$$ Language plpgsql;
-- trigger for ignore tags
Create Trigger tags_ignore_dups
    Before Insert On tags
    For Each Row
    Execute Procedure tags_ignore_duplicate_inserts();