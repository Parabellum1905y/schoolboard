CREATE SEQUENCE postid_seq;
CREATE TABLE POSTS (
	 postId bigint default nextval('postid_seq'),
	 userId bigint NOT NULL,
	 theme varchar (2048),
	 message text,
	 createDate timestamp not null default current_timestamp,
	 PRIMARY KEY( postId ),
	 Foreign Key (userId) references USERS (UserID)
) ;

CREATE TABLE POSTS_TAGS (
    postId bigint not null,
    tagId  bigint not null,
    Foreign Key (postId) references POSTS (postId),
    Foreign Key (tagId) references TAGS (tagId),
    CONSTRAINT tag_for_post UNIQUE (postId, tagId)
);