------------------------------------
-- DB SCHOOL_BOARD
CREATE SEQUENCE user_id_seq;
CREATE TABLE USERS(
	UserId 			bigint default nextval('user_id_seq'),
	userLogin 		varchar(100) NOT NULL UNIQUE,
	userPass 		varchar(400) NOT NULL,
	userMail		varchar(200),
	enabled 		smallint not null default 1,
	rememberMe		smallint default 0,
	Surname 		varchar(100),
	Name 			varchar(100),
	middlename		varchar(100),
	birthDay 		date,
	nick 			varchar(50) NOT NULL,
	avatarFileId	bigint,
	sex				smallint default -1,
	aboutMe			varchar (1024),
	phone			varchar (15),
	RegisterDate 	timestamp NOT NULL default current_timestamp,
	Primary Key(UserID)
);
alter table users add constraint unique_userMail UNIQUE(userMail);

INSERT INTO USERS (UserID, userLogin, userPass, userMail, nick)
values
(1, 'guest', 'guest', 'guest@guest', 'guest'),
(2, 'test', 'test', 'test@test.test', 'test'),
(3, 'admin', 'admin', 'admin@admin.com', 'admin');

SELECT setval('user_id_seq', (SELECT MAX(UserID) FROM USERS));

create SEQUENCE registr_id_seq;
create TABLE USER_REGISTRATION_LOG(
	RegId 		bigint default nextval('registr_id_seq'),
	hashTag		varchar (200) not null,
	ExpireDate	timestamp NOT NULL,
	userId		bigint NOT NULL,
	enabled		smallint not null default 0,
	Primary Key (RegId),
    Foreign Key (userId) references USERS (UserID)
);


create SEQUENCE group_id_seq;
CREATE TABLE USER_GROUPS (
	GroupID 		bigint default nextval('group_id_seq'),
	GroupName 		varchar (100),
	GroupDescr 		varchar (100),
	groupUtfName	varchar (100),
	ENABLED 		smallint not null default 1,
	hidden			smallint not null default 0,
	Primary Key (GroupID)
);

INSERT INTO USER_GROUPS (GroupID, GroupName, GroupDescr, groupUtfName, hidden) values
(1, 'root', 'root', 'key:root', 1),
(2, 'all', 'все', 'key:allusers', 0),
(3, 'moder', 'moderators', 'key:moderusers', 0),
(4, 'developers', 'developers', 'key:devusers', 1),
(5, 'allNoGuest', 'no guests', 'key:noguests', 0);

SELECT setval('group_id_seq', (SELECT MAX(GroupID) FROM USER_GROUPS));

CREATE TABLE MEMBER_CATEGORY(
	CAT_ID 			Smallint NOT NULL UNIQUE,
	CAT_NAME 		varchar (100) NOT NULL,
	ENABLED 		smallint not null default 1,
	Primary Key (CAT_ID)
);

INSERT INTO MEMBER_CATEGORY (CAT_ID, CAT_NAME)
VALUES
(1, 'Пользователь'),
(2, 'Группа');

CREATE SEQUENCE group_rel_id_seq;
CREATE TABLE GROUP_REL (
	groupRelId	 	BIGINT NOT NULL default nextval('group_rel_id_seq'),
	groupId 		BIGINT NOT NULL,
	typeId	 		SMALLINT NOT NULL DEFAULT 1, -- user or group member
	objectId 		BIGINT NOT NULL,
	enabled 		smallint not null default 1,
	Primary Key (groupRelId),
	Foreign Key (groupId) references USER_GROUPS (GroupID),
	Foreign Key (typeId) references MEMBER_CATEGORY (CAT_ID)
);

INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(1, 1, 2),
(2, 1, 1);

INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(1, 1, 3);

INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(2, 1, 1);
INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(2, 1, 3);
SELECT setval('group_rel_id_seq', (SELECT MAX(groupRelId) FROM GROUP_REL));


-- ############################################################################################
-- intersection of two arrays of int
create or replace function array_intersect(a1 bigint[], a2 bigint[]) returns int as $$
declare
    ret int;
begin
    -- The reason for the kludgy NULL handling comes later.
    if a1 is null then
        return 0;
    elseif a2 is null then
        return 0;
    end if;
    if exists (select 1
    from (
        select unnest(a1)
        intersect
        select unnest(a2)
    ) as T) then ret=1;
    else ret=0;
    end if;
    return ret;
end;
$$ language plpgsql;