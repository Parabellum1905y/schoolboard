-- dropdb SCHOOL_BOARD;
-- createdb SCHOOL_BOARD;
CREATE TABLESPACE SCHOOL_BOARD location '/home/postgres/SCHOOL_BOARD';
CREATE DATABASE "SCHOOL_BOARD" OWNER postgres TABLESPACE SCHOOL_BOARD;
-- If you have a dedicated database server
-- with 1GB or more of RAM, a reasonable starting value for shared_buffers is 25% of the memory in your system.
-- see in postgresql.conf
-- SET seq_page_cost = 0.1;
-- SET random_page_cost = 0.1;
-- SET cpu_tuple_cost = 0.05;
-- SET effective_cache_size = '3GB'; 